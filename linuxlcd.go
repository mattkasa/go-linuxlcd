package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"

	"gitlab.com/mattkasa/go-linuxlcd/lcd"
)

type diskList []string

func (d *diskList) String() string {
	return strings.Join(*d, ",")
}

func (d *diskList) Set(value string) error {
	*d = append(*d, value)
	return nil
}

func main() {
	done := make(chan interface{})
	sigs := make(chan os.Signal)

	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		<-sigs
		close(done)
	}()

	var debug bool
	var device, temperature string
	var disks diskList
	var display_interval, stats_interval int

	flag.BoolVar(&debug, "debug", false, "Debug output instead of writing to LCD")
	flag.StringVar(&device, "device", "/dev/lcd", "Device path of the LCD")
	flag.StringVar(&temperature, "temperature", "/sys/class/hwmon/hwmon0/temp1_input", "CPU temperature file path")
	flag.Var(&disks, "disk", "Disk path to include in usage percentage")
	flag.IntVar(&display_interval, "display-interval", 100, "Interval in milliseconds to update display")
	flag.IntVar(&stats_interval, "stats-interval", 1000, "Interval in milliseconds to update system stats")
	flag.Parse()

	local, _ := time.LoadLocation("America/Los_Angeles")

	display := lcd.New(device, disks)
	display.Update()

	statsTicker := time.NewTicker(time.Duration(stats_interval) * time.Millisecond)
	displayTicker := time.NewTicker(time.Duration(display_interval) * time.Millisecond)

	go func() {
		for range statsTicker.C {
			display.Update()
		}
	}()

	go func() {
		for tick := range displayTicker.C {
			display.Time(tick.In(local))
			if debug == true {
				display.Debug()
			} else {
				display.Render()
			}
		}
	}()

	<-done

	statsTicker.Stop()
	displayTicker.Stop()

	if !debug {
		display.Clear()
	}

	fmt.Println("exited")
	os.Exit(0)
}
