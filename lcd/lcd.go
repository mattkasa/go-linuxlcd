package lcd

import (
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"syscall"
	"time"

	"github.com/shirou/gopsutil/v3/cpu"
	"github.com/shirou/gopsutil/v3/disk"
	"github.com/shirou/gopsutil/v3/load"
	"github.com/shirou/gopsutil/v3/mem"
)

type Lcd struct {
	Format               string
	Path                 string
	CpuTemperature       string
	Disks                []string
	cpu, cput, mem, disk float64
	text                 string
}

func New(path string, disks []string) *Lcd {
	var lcd Lcd

	lcd.CpuTemperature = "/sys/class/hwmon/hwmon0/temp1_input"
	lcd.Format = "C%3.0f%% %3.0fC M%3.0f%%D%5.1f%% %s"
	lcd.Path = path

	if len(disks) > 0 {
		lcd.Disks = disks
	} else {
		lcd.Disks = []string{"/"}
	}

	return &lcd
}

func (l *Lcd) String() string {
	return fmt.Sprintf(l.Format, l.cpu, l.cput, l.mem, l.disk, l.text)
}

func (l *Lcd) Debug() {
	value := l.String()
	fmt.Printf("%s\n", value)
}

func (l *Lcd) Clear() {
	blank := []byte("Shutting down...                ")
	write(l.Path, blank)
}

func (l *Lcd) Render() {
	value := l.String()
	bytes := []byte(value)
	write(l.Path, bytes)
}

func (l *Lcd) Time(t time.Time) {
	l.text = t.Format("15:04:05")
}

func (l *Lcd) Update() {
	threads, _ := cpu.Counts(true)
	loadavg, _ := load.Avg()
	l.cpu = (loadavg.Load1 / float64(threads)) * 100

	temp_input, err := ioutil.ReadFile(l.CpuTemperature)
	if err != nil {
		l.cput = 0
	} else {
		line := strings.TrimSpace(string(temp_input))
		temp_float, err := strconv.ParseFloat(line, 64)
		if err != nil {
			l.cput = 0
		} else {
			l.cput = temp_float / 1000
		}
	}

	vm, _ := mem.VirtualMemory()
	l.mem = vm.UsedPercent

	var disk_avail, disk_total float64
	for _, d := range l.Disks {
		usage, _ := disk.Usage(d)
		disk_avail += float64(usage.Free)
		disk_total += float64(usage.Total)
	}
	l.disk = ((disk_total - disk_avail) / disk_total) * 100
}

func write(path string, content []byte) {
	f, err := syscall.Open(path, syscall.O_WRONLY|syscall.O_TRUNC, 0600)
	if err != nil {
		panic(err)
	}

	defer syscall.Close(f)

	_, err = syscall.Write(f, content)
	if err != nil {
		panic(err)
	}

	syscall.Close(f)
}
